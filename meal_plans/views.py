from django.shortcuts import render

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView


from django.views.generic import ListView


from recipes.models import Recipe, Ingredient

from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class MealPlansListView(ListView):
    model = MealPlans
    template_name = "recipes/list.html"
    paginate_by = 2


class MealPlansDetailView(DetailView):
    model = MealPlans
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlans
    template_name = "recipes/new.html"
    fields = [
        "name",
        "description",
        "image",
    ]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlans
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlans
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
